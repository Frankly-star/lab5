%option noyywrap
%{
    #define YY_NO_UNPUT
    #define YY_NO_INPUT

    #include "parser.h"
    #include <ostream>
    #include <fstream>
    using namespace std;

    extern FILE *yyin; 
    extern FILE *yyout;
    extern bool dump_tokens;
	char a[60];
	char b[60];
	int i;
    void DEBUG_FOR_LAB4(std::string s){
        std::string DEBUG_INFO = "[DEBUG LAB4]: \t" + s + "\n";
        fputs(DEBUG_INFO.c_str(), yyout);
    }
%}

OCTAL 0[0-7]*
HEX 0(x|X)[0-9a-zA-Z]+
DECIMIAL ([1-9][0-9]*|0)
ID [[:alpha:]_][[:alpha:][:digit:]_]*
EOL (\r\n|\n|\r)
WHITE [\t ]
LINE "//"[^\n]*
BLOCK "/*"([^\*]|(\*)*[^\*/])*(\*)*"*/" 
%x BLOCKCOMMENT

%%

"int" {
    /*
    * Questions: 
    *   Q1: Why we need to return INT in further labs?
    *   Q2: What is "INT" actually?
    */
    if(dump_tokens)
        DEBUG_FOR_LAB4("INT\tint");
    return INT;
}
"getint" {
    return GETINT;
}
"putint" {
    return PUTINT;
}
"putch" {
    return PUTCH;
}
"getch" {
    return GETCH;
}
"void" {
    if(dump_tokens)
        DEBUG_FOR_LAB4("VOID\tvoid");
    return VOID;
}
"while" {
    if(dump_tokens)
        DEBUG_FOR_LAB4("VOID\tvoid");
    return WHILE;
}
"if" {
    if(dump_tokens)
        DEBUG_FOR_LAB4("IF\tif");
    return IF;
};
"else" {
    if(dump_tokens)
        DEBUG_FOR_LAB4("ELSE\telse");
    return ELSE;
};
"return" {
    if(dump_tokens)
        DEBUG_FOR_LAB4("RETURN\treturn");
    return RETURN;
}
"const" {
    if(dump_tokens)
        DEBUG_FOR_LAB4("RETURN\treturn");
    return CONST;
}

"=" {
    if(dump_tokens)
        DEBUG_FOR_LAB4("ASSIGN\t=");
    return ASSIGN;
}
"<" {
    if(dump_tokens)
        DEBUG_FOR_LAB4("LESS\t<");
    return LESS;
}
">" {
    if(dump_tokens)
        DEBUG_FOR_LAB4("LESS\t<");
    return GREA;
}
">=" {
    if(dump_tokens)
        DEBUG_FOR_LAB4("LESS\t<");
    return GREAASS;
}
"<=" {
    if(dump_tokens)
        DEBUG_FOR_LAB4("LESS\t<");
    return LESSASS;
}
"==" {
    if(dump_tokens)
        DEBUG_FOR_LAB4("LESS\t<");
    return EQ;
}
"!=" {
    if(dump_tokens)
        DEBUG_FOR_LAB4("LESS\t<");
    return UEQ;
}
"+" {
    if(dump_tokens)
        DEBUG_FOR_LAB4("ADD\t+");
    return ADD;
}
"-" {
    if(dump_tokens)
        DEBUG_FOR_LAB4("SUBs\t+");
    return SUB;
}
"!" {
    if(dump_tokens)
        DEBUG_FOR_LAB4("NOT\t+");
    return NOT;
}
"&&" {
    if(dump_tokens)
        DEBUG_FOR_LAB4("AND\t+");
    return AND;
}
"||" {
    if(dump_tokens)
        DEBUG_FOR_LAB4("OR\t+");
    return OR;
}
"*" {
    if(dump_tokens)
        DEBUG_FOR_LAB4("ADD\t+");
    return MUL;
}
"/" {
    if(dump_tokens)
        DEBUG_FOR_LAB4("ADD\t+");
    return DIV;
}
"%" {
    if(dump_tokens)
        DEBUG_FOR_LAB4("ADD\t+");
    return MOD;
}
";" {
    if(dump_tokens)
        DEBUG_FOR_LAB4("SEMICOLON\t;");
    return SEMICOLON;
}
"," {return COMMA;}
"(" {
    if(dump_tokens)
        DEBUG_FOR_LAB4("LPAREN\t(");
    return LPAREN;
}
")" {
    if(dump_tokens)
        DEBUG_FOR_LAB4("RPAREN\t)");
    return RPAREN;
}
"{" {
    if(dump_tokens)
        DEBUG_FOR_LAB4("LBRACE\t{");
    return LBRACE;
}
"}" {
    if(dump_tokens)
        DEBUG_FOR_LAB4("RBRACE\t}");
    return RBRACE;
}

{DECIMIAL} {
    if(dump_tokens)
        DEBUG_FOR_LAB4(yytext);
    yylval.itype = atoi(yytext);
    return INTEGER;
}

{ID} {
    if(dump_tokens)
        DEBUG_FOR_LAB4(yytext);
    char *lexeme;
    lexeme = new char[strlen(yytext) + 1];
    strcpy(lexeme, yytext);
    yylval.strtype = lexeme;
    return ID;
}
{OCTAL} {
	yylval.itype = atoi(yytext);
	for(int j=2;j<yyleng;j++){
	 b[j-2]=yytext[j];
	}
        sscanf(b,"%o",&i);
		sprintf(a,"%d",i);
		yylval.itype = i;
        return INTEGER;
}
{HEX} {
	for(int j=2;j<yyleng;j++){
	 b[j-2]=yytext[j];
	}
        sscanf(b,"%x",&i);
        sprintf(a,"%d",i);
		yylval.itype = i;
        return INTEGER;
}

{EOL} yylineno++;
{WHITE}
{LINE}
{BLOCK}
%%
