#ifndef __AST_H__
#define __AST_H__

#include <fstream>

class SymbolEntry;

class Node
{
private:
    static int counter;
    int seq;
public:
    Node* next;
    Node();
    int getSeq() const {return seq;};
    virtual void output(int level) = 0;
    void setNext(Node* node);
    Node* getNext() { return next; }
};

class ExprNode : public Node
{
protected:
    SymbolEntry *symbolEntry;
public:
    ExprNode(SymbolEntry *symbolEntry) : symbolEntry(symbolEntry){};
};

class BinaryExpr : public ExprNode
{
private:
    int op;
    ExprNode *expr1, *expr2;
public:
    enum {ADD, SUB, MUL,DIV,MOD,AND, OR, LESS,GREA,GREAASS,LESSASS,EQ,UEQ};
    BinaryExpr(SymbolEntry *se, int op, ExprNode*expr1, ExprNode*expr2) : ExprNode(se), op(op), expr1(expr1), expr2(expr2){};
    void output(int level);
};

class UnaryExpr : public ExprNode
{
private:
    int op;
    ExprNode *expr1;
public:
    enum { SUB, NOT};
    UnaryExpr(SymbolEntry *se, int op, ExprNode*expr1) : ExprNode(se), op(op), expr1(expr1){};
    void output(int level);
};

class Constant : public ExprNode
{
public:
    Constant(SymbolEntry *se) : ExprNode(se){};
    void output(int level);
};

class Getint : public ExprNode
{
public:
    Getint(SymbolEntry *se) : ExprNode(se){};
    void output(int level);
};

class Getch : public ExprNode
{
public:
    Getch(SymbolEntry *se) : ExprNode(se){};
    void output(int level);
};

class Id : public ExprNode
{
public:
    Id(SymbolEntry *se) : ExprNode(se){};
    void output(int level);
};


class Func : public ExprNode
{
public:
    ExprNode* a;
    Func(SymbolEntry *se) : ExprNode(se){};
    Func(SymbolEntry *se,ExprNode* a) : ExprNode(se), a(a){};
    void output(int level);
};

class StmtNode : public Node
{};

class Putint : public StmtNode
{
public:
	SymbolEntry* se;
	Putint(SymbolEntry* se) : se(se) {};
	void output(int level);
};
class Putch : public StmtNode
{
public:
	SymbolEntry* se;
	Putch(SymbolEntry* se) : se(se) {};
	void output(int level);
};
class CompoundStmt : public StmtNode
{
private:
    StmtNode *stmt;
public:
    CompoundStmt(StmtNode *stmt) : stmt(stmt) {};
    void output(int level);
};

class SeqNode : public StmtNode
{
private:
    StmtNode *stmt1, *stmt2;
public:
    SeqNode(StmtNode *stmt1, StmtNode *stmt2) : stmt1(stmt1), stmt2(stmt2){};
    void output(int level);
};

class VarDeclStmt: public StmtNode
{
public:
    Id *id;
    ExprNode *expr;
    VarDeclStmt(Id *id, ExprNode *expr) {this->id=id;  this->expr=expr;};
    VarDeclStmt(Id *id) {this->id=id; };
    void output(int level);
};

class ConstDeclStmt: public StmtNode
{
public:
    Id *id;
    ExprNode *expr;
    ConstDeclStmt(Id *id, ExprNode *expr) {this->id=id;  this->expr=expr;};
    ConstDeclStmt(Id *id) {this->id=id; };
    void output(int level);
};

class ConstDefAssign : public StmtNode
{
public:
    Id *id;
    ExprNode *expr;
    ConstDefAssign(Id *id, ExprNode *expr) {this->id=id;  this->expr=expr;};
    void output(int level);
};


class VarDefAssignList : public StmtNode
{
public:
    Id *id;
    Id *id2;
    ExprNode *expr;
    ExprNode *expr2;
    VarDefAssignList(Id *id, Id *id2,ExprNode *expr,ExprNode *expr2){
this->id=id;  
this->id2=id2; 
this->expr=expr;
this->expr2=expr2;
};
    void output(int level);
};




class ConstDefAssignList : public StmtNode
{
public:
    Id *id;
    Id *id2;
    ExprNode *expr;
    ExprNode *expr2;
    ConstDefAssignList(Id *id, Id *id2,ExprNode *expr,ExprNode *expr2){
this->id=id;  
this->id2=id2; 
this->expr=expr;
this->expr2=expr2;
};
    void output(int level);
};


class IfStmt : public StmtNode
{
private:
    ExprNode *cond;
    StmtNode *thenStmt;
public:
    IfStmt(ExprNode *cond, StmtNode *thenStmt) : cond(cond), thenStmt(thenStmt){};
    void output(int level);
};

class WhileStmt : public StmtNode
{
private:
    ExprNode *cond;
    StmtNode *wStmt;
public:
    WhileStmt(ExprNode *cond, StmtNode *wStmt) : cond(cond), wStmt(wStmt){};
    void output(int level);
};


class IfElseStmt : public StmtNode
{
private:
    ExprNode *cond;
    StmtNode *thenStmt;
    StmtNode *elseStmt;
public:
    IfElseStmt(ExprNode *cond, StmtNode *thenStmt, StmtNode *elseStmt) : cond(cond), thenStmt(thenStmt), elseStmt(elseStmt) {};
    void output(int level);
};

class ReturnStmt : public StmtNode
{
private:
    ExprNode *retValue;
public:
    ReturnStmt(ExprNode*retValue) : retValue(retValue) {};
    void output(int level);
};

class AssignStmt : public StmtNode
{
private:
    ExprNode *lval;
    ExprNode *expr;
public:
    AssignStmt(ExprNode *lval, ExprNode *expr) : lval(lval), expr(expr) {};
    void output(int level);
};


class FunctionDef : public StmtNode
{
private:
    SymbolEntry *se;
    StmtNode *stmt;
    StmtNode *stmt2;
public:
    FunctionDef(SymbolEntry *se, StmtNode *stmt,StmtNode *stmt2) : se(se), stmt(stmt),stmt2(stmt2){};
    void output(int level);
};




class Ast
{
private:
    Node* root;
public:
    Ast() {root = nullptr;}
    void setRoot(Node*n) {root = n;}
    void output();
};

#endif
