%code top{
    #include <iostream>
    #include <assert.h>
	#include <string>
    #include "parser.h"
    extern Ast ast;
    int yylex();
    int yyerror( char const * );
}

%code requires {
    #include "Ast.h"
    #include "SymbolTable.h"
    #include "Type.h"
}

%union {
    int itype;
    char* strtype;
    StmtNode* stmttype;
    ExprNode* exprtype;
    Type* type;
}

%start Program
%token <strtype> ID 
%token <itype> INTEGER 
%token IF ELSE WHILE THEN 
%token INT VOID GETINT GETCH PUTINT PUTCH
%token LPAREN RPAREN LBRACE RBRACE SEMICOLON COMMA CONST
%token ADD SUB OR AND LESS ASSIGN MUL DIV MOD GREA GREAASS LESSASS EQ UEQ NOT
%token RETURN

%nterm <stmttype> Stmts Stmt AssignStmt BlockStmt IfStmt WhileStmt ReturnStmt DeclStmt FuncDef ConstDefList ConstDeclStmt ConstDef VarDefList VarDeclStmt VarDef  FuncPa OFuncPas FuncPas Putint Putch 
%nterm <exprtype> Exp AddExp Cond LOrExp PrimaryExp LVal RelExp LAndExp MulExp EqExp UnaryExp FuncRParams
%nterm <type> Type

%precedence THEN 
%precedence ELSE
%%
Program
    : Stmts {
        ast.setRoot($1);
    }
    ;
Stmts
    : Stmt {$$=$1;}
    | Stmts Stmt{
        $$ = new SeqNode($1, $2);
    }
    ;
Stmt
    : AssignStmt {$$=$1;}
    | BlockStmt {$$=$1;}
    | IfStmt {$$=$1;}
    | ReturnStmt {$$=$1;}
    | DeclStmt {$$=$1;}
    | FuncDef {$$=$1;}
	| WhileStmt {$$=$1;}
	| Putint {$$=$1;}
	| Putch {$$=$1;}
    ;
LVal
    : ID {
        SymbolEntry *se;
        se = identifiers->lookup($1);
        $$ = new Id(se);
        delete []$1;
    }
    ;
AssignStmt
    :
    LVal ASSIGN Exp SEMICOLON {
        $$ = new AssignStmt($1, $3);
    }
    ;
BlockStmt
    :   LBRACE 
        {identifiers = new SymbolTable(identifiers);} 
        Stmts RBRACE 
        {
            $$ = new CompoundStmt($3);
            SymbolTable *top = identifiers;
            identifiers = identifiers->getPrev();
            delete top;
        }
	|
	 LBRACE RBRACE
    {
        $$= new CompoundStmt(NULL);
    }
    ;
IfStmt
    : IF LPAREN Cond RPAREN Stmt %prec THEN {
        $$ = new IfStmt($3, $5);
    }
    | IF LPAREN Cond RPAREN Stmt ELSE Stmt {
        $$ = new IfElseStmt($3, $5, $7);
    }
	;
WhileStmt
    : WHILE LPAREN Cond RPAREN Stmt {
        $$ = new WhileStmt($3, $5);
    }
    ;

ReturnStmt
    :
    RETURN Exp SEMICOLON{
        $$ = new ReturnStmt($2);
    }
    ;
Exp
    :
    AddExp {$$ = $1;}
    ;
Cond
    :
    LOrExp {$$ = $1;}
    ;
PrimaryExp
    :
	LPAREN Exp RPAREN {$$ = $2;}
	|
    LVal {
        $$ = $1;
    }
    | 
	INTEGER {
        SymbolEntry *se = new ConstantSymbolEntry(TypeSystem::intType, $1);
        $$ = new Constant(se);
    }
	|
	ID LPAREN RPAREN
	{
		SymbolEntry *se;
        se = identifiers->lookup($1);
		$$ = new Func(se);
        delete []$1;
	}
	|
	ID LPAREN FuncRParams RPAREN
	{
		SymbolEntry *se;
        se = identifiers->lookup($1);
		$$ = new Func(se,$3);
        delete []$1;
	}
	|
	GETINT LPAREN RPAREN
	{
		int a;
		scanf("%d",&a);
        SymbolEntry *se = new ConstantSymbolEntry(TypeSystem::intType, a);
        $$ = new Getint(se);
	}
	|
	GETCH LPAREN RPAREN
	{
		char a;
		int b;
		scanf("%c",&a);
		b=a-'0'+'0';
        SymbolEntry *se = new ConstantSymbolEntry(TypeSystem::intType, b);
        $$ = new Getint(se);
	}
    ;
FuncRParams
    :
    Exp {$$ = $1;}
    |
    FuncRParams COMMA Exp {
        $$ = $1;
        $$->setNext($3);
    }
    ;
UnaryExp
	:
	PrimaryExp{
        $$ = $1;
    }
	|
    ADD UnaryExp
	{
	    $$ = $2;
	}
	|
	NOT UnaryExp
	{
	SymbolEntry *se = new TemporarySymbolEntry(TypeSystem::intType, SymbolTable::getLabel());
        $$ = new UnaryExpr(se, UnaryExpr::NOT, $2);
	}
	|
	SUB UnaryExp
	{
	SymbolEntry *se = new TemporarySymbolEntry(TypeSystem::intType, SymbolTable::getLabel());
        $$ = new UnaryExpr(se, UnaryExpr::SUB, $2);
	}
	;
MulExp
	:
	UnaryExp
	{
	$$ = $1;
	}
	|
	MulExp MUL UnaryExp
	{
		SymbolEntry *se = new TemporarySymbolEntry(TypeSystem::intType, SymbolTable::getLabel());
        $$ = new BinaryExpr(se, BinaryExpr::MUL, $1, $3);
	}
	|
	MulExp DIV UnaryExp
	{
		SymbolEntry *se = new TemporarySymbolEntry(TypeSystem::intType, SymbolTable::getLabel());
        $$ = new BinaryExpr(se, BinaryExpr::DIV, $1, $3);
	}
	|
	MulExp MOD UnaryExp
	{
		SymbolEntry *se = new TemporarySymbolEntry(TypeSystem::intType, SymbolTable::getLabel());
        $$ = new BinaryExpr(se, BinaryExpr::MOD, $1, $3);
	}
	;
AddExp
    :
    MulExp {$$ = $1;}
    |
    AddExp ADD MulExp
    {
        SymbolEntry *se = new TemporarySymbolEntry(TypeSystem::intType, SymbolTable::getLabel());
        $$ = new BinaryExpr(se, BinaryExpr::ADD, $1, $3);
    }
    |
    AddExp SUB MulExp
    {
        SymbolEntry *se = new TemporarySymbolEntry(TypeSystem::intType, SymbolTable::getLabel());
        $$ = new BinaryExpr(se, BinaryExpr::SUB, $1, $3);
    }
    ;
RelExp
    :
    AddExp {$$ = $1;}
    |
    RelExp LESS AddExp
    {
        SymbolEntry *se = new TemporarySymbolEntry(TypeSystem::intType, SymbolTable::getLabel());
        $$ = new BinaryExpr(se, BinaryExpr::LESS, $1, $3);
    }
	|
	RelExp GREA AddExp
    {
        SymbolEntry *se = new TemporarySymbolEntry(TypeSystem::intType, SymbolTable::getLabel());
        $$ = new BinaryExpr(se, BinaryExpr::GREA, $1, $3);
    }
	|
	RelExp LESSASS AddExp
    {
        SymbolEntry *se = new TemporarySymbolEntry(TypeSystem::intType, SymbolTable::getLabel());
        $$ = new BinaryExpr(se, BinaryExpr::LESSASS, $1, $3);
    }
	|RelExp GREAASS AddExp
    {
        SymbolEntry *se = new TemporarySymbolEntry(TypeSystem::intType, SymbolTable::getLabel());
        $$ = new BinaryExpr(se, BinaryExpr::GREAASS, $1, $3);
    }
    ;
EqExp
	:
	RelExp{$$ = $1;}
	|
	EqExp EQ RelExp
	{
	 SymbolEntry *se = new TemporarySymbolEntry(TypeSystem::intType, SymbolTable::getLabel());
     $$ = new BinaryExpr(se, BinaryExpr::EQ, $1, $3);
	}
	|
	EqExp UEQ RelExp
	{
	 SymbolEntry *se = new TemporarySymbolEntry(TypeSystem::intType, SymbolTable::getLabel());
     $$ = new BinaryExpr(se, BinaryExpr::UEQ, $1, $3);
	}
	;
LAndExp
    :
    EqExp {$$ = $1;}
    |
    LAndExp AND EqExp
    {
        SymbolEntry *se = new TemporarySymbolEntry(TypeSystem::intType, SymbolTable::getLabel());
        $$ = new BinaryExpr(se, BinaryExpr::AND, $1, $3);
    }
    ;
LOrExp
    :
    LAndExp {$$ = $1;}
    |
    LOrExp OR LAndExp
    {
        SymbolEntry *se = new TemporarySymbolEntry(TypeSystem::intType, SymbolTable::getLabel());
        $$ = new BinaryExpr(se, BinaryExpr::OR, $1, $3);
    }
    ;
Type
    : INT {
        $$ = TypeSystem::intType;
    }
    | VOID {
        $$ = TypeSystem::voidType;
    }
    ;
DeclStmt
    : VarDeclStmt {$$ = $1;}
	| 
	  ConstDeclStmt{$$ = $1;}
    ;


ConstDeclStmt
    : CONST Type ConstDefList SEMICOLON {$$ = $3;}
    ;
ConstDefList
    : ConstDefList COMMA ConstDef{
	$$=$1;
	$$->setNext($3);
	}
	|
	ConstDef{$$=$1;}
    ;
ConstDef
	:
	ID {
        SymbolEntry* se;
        se = new IdentifierSymbolEntry(TypeSystem::intType, $1, identifiers->getLevel());
        identifiers->install($1, se);
        $$ = new ConstDeclStmt(new Id(se));
        delete []$1;
    }
	|
	ID ASSIGN Exp{
		SymbolEntry* se;
        se = new IdentifierSymbolEntry(TypeSystem::constintType, $1, identifiers->getLevel());
        identifiers->install($1, se);
		delete []$1;
		$$ = new ConstDeclStmt(new Id(se), $3);
	}
	;

VarDeclStmt
    : Type VarDefList SEMICOLON {$$ = $2;}
    ;
VarDefList
    : VarDefList COMMA VarDef{
	$$=$1;
	$1->setNext($3);
	}
	|
	VarDef{$$=$1;}
    ;
VarDef
	:
	ID {
        SymbolEntry* se;
        se = new IdentifierSymbolEntry(TypeSystem::intType, $1, identifiers->getLevel());
        identifiers->install($1, se);
        $$ = new VarDeclStmt(new Id(se));
    }
	|
	ID ASSIGN Exp{
		SymbolEntry* se;
        se = new IdentifierSymbolEntry(TypeSystem::constintType, $1, identifiers->getLevel());
        identifiers->install($1, se);
		delete []$1;
		$$ = new VarDeclStmt(new Id(se), $3);
	}
	;
FuncDef
    :
    Type ID {
        Type *funcType;
        funcType = new FunctionType($1,{});
        SymbolEntry *se = new IdentifierSymbolEntry(funcType, $2, identifiers->getLevel());
        identifiers->install($2, se);
        identifiers = new SymbolTable(identifiers);
    }
    LPAREN OFuncPas RPAREN 
    BlockStmt
    {
        SymbolEntry *se;
        se = identifiers->lookup($2);
        assert(se != nullptr);
		$$ = new FunctionDef(se, $7,$5);
        SymbolTable *top = identifiers;
        identifiers = identifiers->getPrev();
        delete top;
        delete []$2;
    }
	;
OFuncPas
	:
	FuncPas{$$=$1;};
	| %empty {$$ = NULL;}
FuncPas
	:
	FuncPas COMMA FuncPa{
		$$ = $1;
        $$->setNext($3);
	}
	|
	FuncPa{$$=$1;}
	;
FuncPa:
	Type ID{ 
	SymbolEntry* se;
    se = new IdentifierSymbolEntry(TypeSystem::intType, $2, identifiers->getLevel());
    identifiers->install($2, se);
    $$ = new VarDeclStmt(new Id(se),NULL);
    delete []$2;}
	|
	Type ID ASSIGN Exp{
		SymbolEntry* se;
        se = new IdentifierSymbolEntry(TypeSystem::constintType, $2, identifiers->getLevel());
        identifiers->install($2, se);
		delete []$1;
		$$ = new VarDeclStmt(new Id(se), $4);
	}
	;

Putint:
	PUTINT LPAREN ID RPAREN SEMICOLON
	{
	SymbolEntry *se;
    se = identifiers->lookup($3);
    $$ = new Putint(se);
    delete []$3;
	}
	|
	PUTINT LPAREN INTEGER RPAREN SEMICOLON
	{
	SymbolEntry *se = new ConstantSymbolEntry(TypeSystem::intType, $3);
    $$ = new Putint(se);
	}
	;
Putch:
	PUTCH LPAREN ID RPAREN SEMICOLON
	{
	SymbolEntry *se;
    se = identifiers->lookup($3);
    $$ = new Putch(se);
    delete []$3;
	}
	|
	PUTCH LPAREN INTEGER RPAREN SEMICOLON
	{
	SymbolEntry *se = new ConstantSymbolEntry(TypeSystem::intType, $3);
    $$ = new Putch(se);
	}
	;
%%

int yyerror(char const* message)
{
    std::cerr<<message<<std::endl;
    return -1;
}
